Source: python-bottle-sqlite
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>
Build-Depends: debhelper-compat (= 13), dh-python,
 python3-all,
 python3-setuptools,
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: http://bottlepy.org/docs/dev/plugins/sqlite.html
Vcs-Git: https://salsa.debian.org/python-team/packages/python-bottle-sqlite.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-bottle-sqlite

Package: python3-bottle-sqlite
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends},
 python3-bottle,
Recommends: ${python3:Recommends}
Suggests: ${python3:Suggests}
Description: SQLite3 integration for Bottle - Python 3.x
 Bottle-sqlite is a plugin that integrates SQLite3 with your Bottle
 application. It automatically connects to a database at the beginning of a
 request, passes the database handle to the route callback and closes the
 connection afterwards.
 .
 To automatically detect routes that need a database connection, the plugin
 searches for route callbacks that require a `db` keyword argument
 (configurable) and skips routes that do not. This removes any overhead for
 routes that don't need a database connection.
 .
 This package provides the Python 3.x module.
